## N:1 (다대일) 연관관계 
### 라이브러리
- Spring Boot DevTools
- Lombok
- Spring Web
- Thymeleaf
- Spring Data JPA

### 알아둘것
- 연관관계 맺어있는 건 조인되기 때문에(`Eager`) 
  - 우리는 `Lazy` 하자!!